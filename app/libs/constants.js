module.exports = Object.freeze({
  ENUM: {
    PASS_STATUS: {
      DEFAULT: 'WD1',
      ACTIVE: 'WD2',
      INACTIVE: 'WD3',
      EXPIRED: 'WD4',
    },
    PIC_FLAG: {
      YES: true,
      NO: false,
    },
    STATUS: {
      ACTIVE: 'ACTIVE',
      INACTIVE: 'INACTIVE',
      DELETED: 'DELETED',
      BLACKLISTED: 'BLACKLISTED',
      BLOCKED: 'BLOCKED',
    },
    SYS_STATUS: {
      NORMAL: 'NORMAL',
      PENDING: 'PENDING',
    },
    VALID_FLAG: {
      VALID: 'VALID',
      INVALID: 'INVALID',
    },
    LIST_TYPE: {
      ASSET: 'ASSET',
      DEBT: 'DEBT',
      EQUITY: 'EQUITY',
      P_L: 'P_L',
    },
    PART_OF_BIZ_FLAG: {
      YES: true,
      NO: false,
    },
    CUST_TYPE: {
      REGULAR: 'REGULAR',
      NON_REGULAR: 'NON_REGULAR',
    },
    ACC_TYPE_FLAG: {
      MAIN_ACCOUNT: 'MAIN_ACCOUNT',
      ADDITIONAL_ACCOUNT: 'ADDITIONAL_ACCOUNT',
    },
    IN_PROGRESS_FLAG: {
      YES: true,
      NO: false,
    },
    DEPRECIATION_METHOD: {
      STRAIGHT_LINE: 'STRAIGHT_LINE',
      MULTIPLE_DECREASE: 'MULTIPLE_DECREASE',
    },
    SALES_TYPE: {
      INVENTORY_GOOD: 'S01',
      FIXED_ASSET: 'S02',
      SERVICES: 'S03',
      RENT: 'S04',
    },
    PAYMENT_TYPE: {
      CASH: 'CASH',
      NON_CASH: 'NON_CASH',
    },
    MULTI_PAYMENT: {
      YES: true,
      NO: false,
    },
    SHIPMENT_TERM: {
      FRANCO: 'FRANCO',
      LOCO: 'LOCO',
      FOB_SHIPPING_POINT: 'FOB_SHIPPING_POINT',
      FOB_DESTINATION: 'FOB_DESTINATION',
    },
    ITEM_TYPE: {
      PROD_GOODS: 'P1',
      PROD_SERVICES: 'P2',
    },
    ESTIMATED_FLAG: {
      YES: true,
      NO: false,
    },
    SELLING_FLAG: {
      YES: true,
      NO: false,
    },
    INVENTORY_METHOD: {
      FIFO: 'INVG1',
      AVG: 'INVG2',
    },
    SYS_TYPE: {
      AUTO: 'AUTO',
      CONFIG: 'CONFIG',
      DB: 'DB',
      API: 'API',
    },
    NOTIF_TYPE: {
      REGULAR: 'REGULAR',
      UPDATE: 'UPDATE',
      IMPORTANT: 'IMPORTANT',
      FINANCIAL: 'FINANCIAL',
      REMINDER: 'REMINDER',
    },
    BIZ_ACT_TYPE: {
      INDIVIDUAL: 'B1',
      RENT: 'B2',
      SERVICE: 'B3',
      RESTAURANT: 'B4',
      MANUFACTURE: 'B5',
    },
    TAX_AVAIL_FLAG: {
      YES: true,
      NO: false,
    },
    PKP_FLAG: {
      YES: true,
      NO: false,
    },
    FINANCIAL_REPORT_AWARENESS: {
      YES: true,
      NO: false,
    },
    FINANCIAL_REPORT_FLAG: {
      YES: true,
      NO: false,
    },
    SERVICE_TYPE: {
      RENT: 'SVC01',
      SERVICES: 'SVC02',
    },
    INHOUSE_PRODUCTION: {
      YES: true,
      NO: false,
    },
  },
});
