const Joi = require('@hapi/joi');

const { ENUM } = require('../../libs/constants');
const { errorResponse, successResponse } = require('../../helpers/common');
const { registerCustomer } = require('../../action/customer/register');

const register = async (req, res) => {
  try {
    const bodySchema = Joi.object({
      fullName: Joi.string().required(),
      email: Joi.string().email().required(),
      passwd: Joi.string().required(),
      phoneNo: Joi.string().required(),
      jobTitle: Joi.string().required(),
      companyName: Joi.string().required(),
      status: Joi.valid(...Object.values(ENUM.STATUS)),
      sysStatus: Joi.valid(...Object.values(ENUM.SYS_STATUS)).required(),
      createdBy: Joi.number().integer().required(),
      updatedBy: Joi.number().integer().required(),
    }).required();

    await bodySchema.validateAsync(req.body);

    const registerCustomerPayload = req.body;
    const registerCustomerResult = await registerCustomer(registerCustomerPayload);
    return successResponse(
      res,
      registerCustomerResult,
      200,
    );
  } catch (error) {
    return errorResponse(
      res,
      error,
      500,
    );
  }
};

module.exports = (router) => {
  router.post('/register', register);
};
