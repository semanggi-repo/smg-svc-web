/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('funnelData', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
			field: 'Id'
		},
		'fullName': {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'FullName'
		},
		'email': {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Email'
		},
		'passwd': {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Passwd'
		},
		'phoneNo': {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Phone_No'
		},
		'jobTitle': {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Job_Title'
		},
		'companyName': {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'Company_Name'
		},
		'status': {
			type: DataTypes.ENUM("ACTIVE","BLACKLISTED","BLOCKED","DELETED","INACTIVE"),
			allowNull: false,
			field: 'Status'
		},
		'sysStatus': {
			type: DataTypes.ENUM("NORMAL","PENDING"),
			allowNull: false,
			field: 'Sys_Status'
		},
		'createdBy': {
			type: DataTypes.INTEGER,
			allowNull: false,
			field: 'Created_By'
		},
		'updatedBy': {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'Updated_By'
		}
	}, {
		tableName: 'Funnel_Data',
		createdAt: 'Created_Date',
		updatedAt: 'Updated_Date',
		timestamps: true
	});
};
