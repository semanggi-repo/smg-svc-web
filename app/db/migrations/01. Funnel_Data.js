const { ENUM } = require('../../libs/constants');

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('Funnel_Data', {
    Id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    FullName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Email: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Passwd: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Phone_No: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Job_Title: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Company_Name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Status: {
      type: Sequelize.ENUM(...Object.values(ENUM.STATUS)),
      allowNull: false,
    },
    Sys_Status: {
      type: Sequelize.ENUM(...Object.values(ENUM.SYS_STATUS)),
      allowNull: false,
    },
    Created_Date: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    Created_By: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    Updated_Date: {
      type: Sequelize.DATE,
    },
    Updated_By: {
      type: Sequelize.INTEGER,
    },
  }),
  down: queryInterface => queryInterface.dropTable('Funnel_Data'),
};
