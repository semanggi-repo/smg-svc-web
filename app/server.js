require('dotenv').config();
const express = require('express');
const enrouten = require('express-enrouten');
const cuid = require('cuid');
const config = require('../config');

const app = express();
const port = config.get('PORT') || 3000;
global.Helpers = require('./helpers/common');
global.CustomStatusCode = require('./helpers/enum').customStatusCode;

// Embedd RequestId
app.use((req, res, next) => {
  req.requestId = cuid();
  next();
});

// Body parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Logger
app.set('etag', false);

// Routing
app.use('/', enrouten({ directory: 'routes' }));
// app.use('/docs', enrouten({ directory: '../docs' }));

// Not Found handler
app.use('*', (req, res) => {
  res.status(404).json({
    message: 'Resource not found.',
  });
});

// Error handler
app.use((err, req, res) => {
  res.status(500).json(err);
});

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`App listening on port ${port}`);
});


module.exports = app;
