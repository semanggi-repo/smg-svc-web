/* eslint-disable import/no-unresolved */
const {
  funnelData,
} = require('../../db/models/Postgres')();

const registerCustomer = payload => new Promise(async (res, rej) => {
  try {
    const payloadData = payload;
    payloadData.createdDate = new Date();
    payloadData.updatedDate = new Date();

    const result = await funnelData.create(payloadData);
    return res(result);
  } catch (error) {
    return rej(error);
  }
});

module.exports = {
  registerCustomer,
};
