/* global before after */
const supertest = require('supertest');
const { assert } = require('chai');
const cuid = require('cuid');
const app = require('../app/server');

const server = supertest(app);

const dataTest = {
  requestId: cuid(),
};

before((done) => {
  // TODO: Seed Data
  done();
});

require('./ping.test')(server, assert, dataTest);

after(async () => {
  try {
    // TODO: Delete After Test
  } catch (error) {
    throw error;
  }
});
